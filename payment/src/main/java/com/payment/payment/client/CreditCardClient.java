package com.payment.payment.client;


import com.payment.payment.models.dto.CreditCard;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.Optional;

@FeignClient(name="creditcard", configuration = CreditCardClientConfiguration.class)
public interface CreditCardClient {
    @GetMapping("/cartao/{id}")
    Optional<CreditCard> findCreditCardCustomerById(@PathVariable Long id);
}