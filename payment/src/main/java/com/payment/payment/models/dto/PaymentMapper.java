package com.payment.payment.models.dto;

import com.payment.payment.models.Payment;
import org.springframework.stereotype.Component;

@Component
public class PaymentMapper {
    public Payment toPayment(PaymentRequest paymentRequest) {
        Payment payment = new Payment();

        payment.setCreditCardId(paymentRequest.getCreditCardId());
        payment.setDescription(paymentRequest.getDescription());
        payment.setValue(paymentRequest.getValue());

        return payment;
    }
}
