package com.customer.customer.controller;

import com.customer.customer.models.Customer;
import com.customer.customer.models.dto.CreateCustomerRequest;
import com.customer.customer.models.dto.CustomerMapper;
import com.customer.customer.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/cliente")
public class CustomerController {

    @Autowired
    private CustomerService service;

    @Autowired
    private CustomerMapper mapper;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Customer create(@RequestBody CreateCustomerRequest createCustomerRequest) {
        Customer customer = mapper.toCustomer(createCustomerRequest);

        customer = service.create(customer);

        return customer;
    }

    @GetMapping("/{id}")
    public Customer getById(@PathVariable Long id) {
        return service.getById(id);
    }

}
